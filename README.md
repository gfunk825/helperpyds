# helperPyDS

This repo contains functions to help visualize data from pandas dataframes.

makeHistoPages(df) creates pages of histograms for all fields in the dataframe df

makeVioPagesBySeg creates pages of violin plots for all fields in a dataframe, df, with a separate distribution for each label/segment. 