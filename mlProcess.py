import subprocess
import os
import math
import random
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats as stats
import json
import requests
import glob, os
import operator
from matplotlib.backends.backend_pdf import PdfPages

def underSample(X_train,y_train,sampleFactor,singleGroup = None):
    df_train = X_train
    df_train=df_train.assign(target=y_train)
    count_class = dict(df_train.target.value_counts())
    classMinCount = list(count_class.keys())[-1]
    minCount = list(count_class.values())[-1]
    print(count_class)
    print('Smallest Class: ' + str(classMinCount))
    print('Count: ' + str(minCount))
    splitDFs = {}
    for k in count_class.keys():
        dfTemp = pd.DataFrame()
        if k == classMinCount:
            dfTemp = df_train[df_train['target'] == k]
        else:
            if singleGroup != None:
                if k == singleGroup:
                    dfTemp = df_train[df_train['target'] == k].sample(int(sampleFactor*count_class[k]))
                else:
                    dfTemp = df_train[df_train['target'] == k]
            else:
                dfTemp = df_train[df_train['target'] == k].sample(min(count_class[k],int(sampleFactor*minCount)))
        splitDFs[k] = dfTemp
    df_test_under = pd.concat(splitDFs.values(), axis=0)
    print('Under-sampling:')
    print(df_test_under.target.value_counts())
    X_train=df_test_under.drop(['target'],axis=1)
    y_train=df_test_under['target']
    return X_train,y_train

def overSample(X_train,y_train,sampleFactor):
    df_train = X_train
    df_train=df_train.assign(target=y_train)
    count_class = dict(df_train.target.value_counts())
    classMaxCount = list(count_class.keys())[0]
    maxCount = list(count_class.values())[0]
    classMinCount = list(count_class.keys())[-1]
    minCount = list(count_class.values())[-1]
    print(count_class)
    print('Largest Class: ' + str(classMaxCount))
    print('Count: ' + str(maxCount))
    print('Smallest Class: ' + str(classMinCount))
    print('Count: ' + str(minCount))
    splitDFs = {}
    for k in count_class.keys():
        dfTemp = pd.DataFrame()
        if k == classMaxCount:
            dfTemp = df_train[df_train['target'] == k]
        else:
            dfTemp = df_train[df_train['target'] == k].sample(int(sampleFactor*maxCount), replace=True)
        splitDFs[k] = dfTemp
    df_test_under = pd.concat(splitDFs.values(), axis=0)
    print('Over-sampling:')
    print(df_test_under.target.value_counts())
    X_train=df_test_under.drop(['target'],axis=1)
    y_train=df_test_under['target']
    return X_train,y_train
