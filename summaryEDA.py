import subprocess
import os
import math
import random
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats as stats
import json
import requests
import glob, os
import operator
from sklearn.metrics import confusion_matrix,classification_report
from sklearn.metrics import roc_curve, auc, matthews_corrcoef, precision_recall_curve

from statsmodels.distributions.empirical_distribution import ECDF
from matplotlib.backends.backend_pdf import PdfPages

# make simple histograms for all features in dataframe
def makeHistoPages(df, fileName, bins=15, cut = None, xLim = None, xLimPerc = None, vLine = None, function = None):
    with PdfPages(fileName) as pdf:
        cols = df.columns
        pages = math.ceil(df.shape[1]/12.)
        h = 0
        for p in range(pages):
            plt.rcParams.update({'font.size': 5})
            fhists, hists = plt.subplots(4,3)
            for i in range(4):
                for j in range(3):
                    if h < df.shape[1]:
                        print(df.columns[h])
                        tempSer = df.iloc[:,h]
                        if cut!=None:
                            tempSer = tempSer[tempSer>cut]
                        if xLimPerc!=None:
                            if (len(tempSer.unique())>4):
                                try:
                                    xlpCut = np.percentile(np.array(tempSer),xLimPerc)
                                    tempSer = tempSer[tempSer<xlpCut]
                                    hists[i,j].set_xlim([0.,xlpCut])
                                except:
                                    print('unusable limit')
                        elif xLim != None:
                            tempSer = tempSer[tempSer<xLim]
                            hists[i,j].set_xlim([0.,xLim])
                        if function != None:
                            f = function(np.array(tempSer))
                            X = f.x
                            Y = f.y
                            hists[i,j].plot(X,Y)
                        else:
                            if (len(tempSer.unique())<3):
                                #hists[i,j].hist(np.array(tempSer),bins=2)
                                hists[i,j].hist(np.array(tempSer),bins=bins)
                            else:
                                hists[i,j].hist(np.array(tempSer),bins=bins)
                        if vLine != None:
                            try:
                                hists[i,j].axvline(x=np.percentile(np.array(tempSer),vLine), color='r', linewidth=2)
                            except:
                                print('unusable vline value')
                        hists[i,j].set_xlabel(cols[h])
                        h+=1
            fhists.tight_layout()
            pdf.savefig()
            plt.close()

# make simple histograms for all features in dataframe
def makeHistoPages2D(df, fileName, d2Col, bins=15, cut = None, xLim = None, xLimPerc = None, vLine = None, yLimPerc=None):
    with PdfPages(fileName) as pdf:
        cols = df.columns
        pages = math.ceil(df.shape[1]/12.)
        h = 0
        for p in range(pages):
            plt.rcParams.update({'font.size': 5})
            fhists, hists = plt.subplots(4,3)
            for i in range(4):
                for j in range(3):
                    if h < df.shape[1]:
                        print(df.columns[h])
                        tempSer = df.iloc[:,h]
                        tempSerD2 = df[d2Col]
                        if cut!=None:
                            cutIndex = tempSer>cut
                            tempSer = tempSer[cutIndex]
                            tempSerD2 = tempSerD2[cutIndex]
                        if yLimPerc!=None:
                            ylpCut = np.percentile(np.array(tempSerD2),yLimPerc)
                            limIndex = tempSerD2<ylpCut
                            tempSer = tempSer[limIndex]
                            tempSerD2 = tempSerD2[limIndex]
                        if xLimPerc!=None:
                            if (len(tempSer.unique())>4):
                                try:
                                    xlpCut = np.percentile(np.array(tempSer),xLimPerc)
                                    limIndex = tempSer<xlpCut
                                    tempSer = tempSer[limIndex]
                                    tempSerD2 = tempSerD2[limIndex]
                                #hists[i,j].set_xlim([0.,xlpCut])
                                except:
                                    print('unusable limit')
                        elif xLim != None:
                            limIndex = tempSer<xLim
                            tempSer = tempSer[limIndex]
                            tempSerD2 = tempSerD2[limIndex]
                            hists[i,j].set_xlim([0.,xLim])
                        if (len(tempSer.unique())<3):
                            bins = 2
                        hists[i,j].hist2d(np.array(tempSer),np.array(tempSerD2),bins=bins,cmap='cool')
                        if vLine != None:
                            try:
                                hists[i,j].axvline(x=np.percentile(np.array(tempSer),vLine), color='r', linewidth=2)
                            except:
                                print('unusable vline value')
                        hists[i,j].set_xlabel(cols[h])
                        hists[i,j].set_ylabel(d2Col)
                        h+=1
            fhists.tight_layout()
            pdf.savefig()
            plt.close()

# make simple histograms for all features with custom function for vertical line placement
def makeHistoPagesTurnOn(df, fileName, turnOnDict, bins=15, cut = None, xLim = None, xLimPerc = None, function = None, ptext=None):
    print('Making Plots:')
    with PdfPages(fileName) as pdf:
        cols = df.columns
        pages = math.ceil(df.shape[1]/12.)
        h = 0
        for p in range(pages):
            plt.rcParams.update({'font.size': 5})
            fhists, hists = plt.subplots(4,3)
            for i in range(4):
                for j in range(3):
                    if h < df.shape[1]:
                        print(df.columns[h])
                        if ptext!=None:
                            hists[i,j].text(0.35, 0.9, ptext,transform=hists[i,j].transAxes,color='green')
                        if cut!=None:
                            arr = np.array(df.iloc[:,h][df.iloc[:,h]>cut])
                        else:
                            arr = np.array(df.iloc[:,h])
                        if xLimPerc != None and len(arr)>0:
                            hists[i,j].set_xlim([0.,np.percentile(arr,xLimPerc)])
                        elif xLim != None:
                            hists[i,j].set_xlim([0.,xLim])
                        if function != None:
                            f = function(arr)
                            X = f.x
                            Y = f.y
                            hists[i,j].plot(X,Y)
                        else:
                            hists[i,j].hist(arr,bins=bins)
                        try:
                            hists[i,j].axvline(x=turnOnDict[cols[h]], color='r', linewidth=2)
                        except:
                            print('No vLine')
                        hists[i,j].set_xlabel(cols[h])
                        h+=1
            fhists.tight_layout()
            pdf.savefig()
            plt.close()

def ddxECDF(arr):
    ecdf = ECDF(arr)
    xL = []
    dydxL = []
    for i in range(len(ecdf.x)-step):
        dy = ecdf.y[i+step]-ecdf.y[i]
        dx = ecdf.x[i+step]-ecdf.x[i]
        dydx = dy/dx
        if dx>0. and dydx<100.:
            x = ecdf.x[i]+(dx/2.)
            dydxL.append(dydx)
            xL.append(x)
    LISTS = [np.array(xL),np.array(dydxL)]
    class returned:
        def __init__(self, L):
            self.x = L[0]
            self.y = L[1]
    return returned(LISTS)

def set_axis_style(ax, labels):
    ax.get_xaxis().set_tick_params(direction='out')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_xticks(np.arange(1, len(labels) + 1))
    ax.set_xticklabels(labels)
    ax.set_xlim(0.25, len(labels) + 0.75)

#plot violin plots (or fraction of boolean categories with poisson errors) over multiple segments for all features in dataframe
def makeVioPagesBySeg(df,outFile,segCol,segDict):
    with PdfPages(outFile) as pdf:
        cols = df.columns
        pages = math.ceil(df.shape[1]/6.)
        h = 0
        for p in range(pages):
            plt.rcParams.update({'font.size': 5})
            plt.rc('xtick',labelsize=3)
            fhists, hists = plt.subplots(2,3)
            for i in range(2):
                for j in range(3):
                    if h < df.shape[1]:
                        print(df.columns[h])
                        arr = [np.array(df[df[df.columns[h]].notnull()][df[segCol]==seg].iloc[:,h]) for seg in segDict.keys()]
                        means = [set.mean() for set in arr]
                        inds = np.arange(1, len(means) + 1)
                        hists[i,j].set_xlabel(cols[h])
                        if min([len(set) for set in arr])>0:
                            if max([max(set) for set in arr]) > 1.:
                                vio = hists[i,j].violinplot(arr,showmeans=False,showmedians=False,showextrema=False)
                                up = [np.quantile(set, 0.75) for set in arr]
                                down = [np.quantile(set, 0.25) for set in arr]
                                up2p5Sig = min([(set.mean()+2.5*set.std()) for set in arr])
                                up95Perc = min([(np.quantile(set, 0.95)) for set in arr])
                                for pc in vio['bodies']:
                                    pc.set_facecolor('#D43F3A')
                                    pc.set_alpha(1)
                                hists[i,j].scatter(inds, means, marker='o', color='black', s=7, zorder=3)
                                hists[i,j].vlines(inds, down, up, color='k', linestyle='-', lw=1)
                                hists[i,j].set_ylim([0., max(up2p5Sig,up95Perc)])
                            elif max([len(np.unique(set)) for set in arr]) <= 2:
                                vio = hists[i,j].violinplot(arr,showmeans=False,showmedians=False,showextrema=False)
                                up = [(set.sum()+math.sqrt(set.sum()))/len(set) for set in arr]
                                down = [(set.sum()-math.sqrt(set.sum()))/len(set) for set in arr]
                                up2p5Sig = max([(set.sum()+2.5*math.sqrt(set.sum()))/len(set) for set in arr])
                                down2p5Sig = min([(set.sum()-2.5*math.sqrt(set.sum()))/len(set) for set in arr])
                                for pc in vio['bodies']:
                                    pc.set_facecolor('white')
                                    pc.set_alpha(1)
                                hists[i,j].scatter(inds, means, marker='o', color='black', s=7, zorder=3)
                                hists[i,j].vlines(inds, down, up, color='k', linestyle='-', lw=1)
                                hists[i,j].set_ylim([down2p5Sig, up2p5Sig])
                        set_axis_style(hists[i,j], [segDict[seg] for seg in segDict.keys()])
                        h+=1
            fhists.tight_layout()
            pdf.savefig()
            plt.close()

#plot histograms over multiple segments for all features in dataframe
def makeHistoPagesBySeg(df,outFile,segCol,segDict, ecdf = False, bins = 25):
    colors = ['black','r','g','y']
    style = ['stepfilled','step','step','step']
    with PdfPages(outFile) as pdf:
        cols = df.columns
        pages = math.ceil(df.shape[1]/6.)
        h = 0
        for p in range(pages):
            plt.rcParams.update({'font.size': 5})
            plt.rc('xtick',labelsize=4)
            fhists, hists = plt.subplots(2,3)
            for i in range(2):
                for j in range(3):
                    if h < df.shape[1]:
                        print(df.columns[h])
                        arrTups = [(seg,np.array(df[df[df.columns[h]].notnull()][df[segCol]==seg].iloc[:,h])) for seg in segDict.keys()]
                        arr = [a[1] for a in arrTups]
                        means = [set.mean() for set in arr]
                        inds = np.arange(1, len(means) + 1)
                        hists[i,j].set_xlabel(cols[h])
                        if max([max(set) for set in arr]) > 1.:
                            up = [np.quantile(set, 0.75) for set in arr]
                            down = [np.quantile(set, 0.25) for set in arr]
                            up2p5Sig = min([(set.mean()+2.5*set.std()) for set in arr])
                            up98Perc = min([(np.quantile(set, 0.98)) for set in arr])
                            c = 0
                            for segArr in arrTups:
                                if ecdf:
                                    f = ECDF(segArr[1])
                                    hists[i,j].plot(f.x,f.y,color=colors[c])
                                    hists[i,j].set_xlim([0,up98Perc])
                                else:
                                    tempHist = hists[i,j].hist(segArr[1],density=1,histtype=style[c],range=(0.,up98Perc),color=colors[c])
                                c+=1
                            hists[i,j].legend([tup[0] for tup in arrTups])
                        elif max([len(np.unique(set)) for set in arr]) <= 2:
                            vio = hists[i,j].violinplot(arr,showmeans=False,showmedians=False,showextrema=False)
                            up = [(set.sum()+math.sqrt(set.sum()))/len(set) for set in arr]
                            down = [(set.sum()-math.sqrt(set.sum()))/len(set) for set in arr]
                            up2p5Sig = max([(set.sum()+2.5*math.sqrt(set.sum()))/len(set) for set in arr])
                            down2p5Sig = min([(set.sum()-2.5*math.sqrt(set.sum()))/len(set) for set in arr])
                            for pc in vio['bodies']:
                                pc.set_facecolor('white')
                                pc.set_alpha(1)
                            hists[i,j].scatter(inds, means, marker='o', color='black', s=7, zorder=3)
                            hists[i,j].vlines(inds, down, up, color='k', linestyle='-', lw=1)
                            hists[i,j].set_ylim([down2p5Sig, up2p5Sig])
                            set_axis_style(hists[i,j], [segDict[seg] for seg in segDict.keys()])
                        h+=1
            fhists.tight_layout()
            pdf.savefig()
            plt.close()

#plot bar plots over multiple categories for all features in dataframe
def makeBarPagesBySeg(df,outFile,segCol,segDict):
    with PdfPages(outFile) as pdf:
        cols = df.columns
        pages = math.ceil(df.shape[1]/4.)
        h = 0
        for p in range(pages):
            plt.rcParams.update({'font.size':4})
            fhists, hists = plt.subplots(2,2)
            for i in range(2):
                for j in range(2):
                    if h < df.shape[1]:
                        colValDictUnsorted = df[df.columns[h]].value_counts().to_dict()
                        colValDictBlank = {k:0 for k in sorted(colValDictUnsorted.keys())}
                        colValDictLen = len(colValDictBlank)
                        if colValDictLen < 10 and colValDictLen>0:
                            print(df.columns[h])
                            arr = {seg:df[df[df.columns[h]].notnull()][df[segCol]==seg].iloc[:,h].value_counts().to_dict() for seg in segDict.keys()}
                            hists[i,j].set_xlabel(cols[h])
                            nCats = len(segDict)
                            barWidth = 1./(nCats+1.)
                            w = 0
                            for seg,d in arr.items():
                                print(d)
                                if len(d)>0:
                                    tempDict = colValDictBlank.copy()
                                    for k in d.keys():
                                        tempDict[k] = d[k]
                                    hists[i,j].bar(np.arange(len(tempDict))+w*barWidth, list(tempDict.values()),barWidth, label=segDict[seg])
                                    w+=1
                            hists[i,j].set_xticks(range(colValDictLen))
                            hists[i,j].set_xticklabels(list(colValDictBlank.keys()))
                            hists[i,j].legend()
                            h+=1
                        else:
                            h+=1
                            continue
            fhists.tight_layout()
            pdf.savefig()
            plt.close()


# plot performance over any 1 parameter in gridsearch. can only vary one parameter for consistent plots
def GridSearch_table_plot(grid_clf, param_name, outPlot,
                          num_results=15,
                          negative=True,
                          graph=True,
                          display_all_params=True):
    from matplotlib      import pyplot as plt
    from IPython.display import display
    import pandas as pd
    
    clf = grid_clf.best_estimator_
    clf_params = grid_clf.best_params_
    if negative:
        clf_score = -grid_clf.best_score_
    else:
        clf_score = grid_clf.best_score_
    clf_stdev = grid_clf.cv_results_['std_test_score'][grid_clf.best_index_]
    cv_results = grid_clf.cv_results_

    print("best parameters: {}".format(clf_params))
    print("best score:      {:0.5f} (+/-{:0.5f})".format(clf_score, clf_stdev))
    if display_all_params:
        import pprint
        pprint.pprint(clf.get_params())

    # pick out the best results
    # =========================
    scores_df = pd.DataFrame(cv_results).sort_values(by='rank_test_score')

    best_row = scores_df.iloc[0,:]
    if negative:
        best_mean = -best_row['mean_test_score']
    else:
        best_mean = best_row['mean_test_score']
    best_stdev = best_row['std_test_score']
    best_param = best_row['param_' + param_name]

    # display the top 'num_results' results
    # =====================================
    performanceDF = pd.DataFrame(cv_results)
    summaryParamList = [c for c in performanceDF.columns if 'param_' in c]
    summaryColList = ['mean_test_score','std_test_score','mean_train_score','std_train_score']
    print(performanceDF[['rank_test_score']+summaryParamList+summaryColList].sort_values(['rank_test_score'],ascending=True))
    
    # plot the results
    # ================
    scores_df = scores_df.sort_values(by='param_' + param_name)
    
    if negative:
        means = -scores_df['mean_test_score']
    else:
        means = scores_df['mean_test_score']
    stds = scores_df['std_test_score']
    params = scores_df['param_' + param_name]
    # plot
    if graph:
        plt.figure(figsize=(8, 8))
        plt.errorbar(params, means, yerr=stds)
        
        plt.axhline(y=best_mean + best_stdev, color='red')
        plt.axhline(y=best_mean - best_stdev, color='red')
        plt.plot(best_param, best_mean, 'or')
        
        plt.title(param_name + " vs Score\nBest Score {:0.5f}".format(clf_score))
        plt.xlabel(param_name)
        plt.ylabel('Score')
        plt.savefig(outPlot, bbox_inches='tight')
        plt.close()


def modelSummaryPlots(dir, gridSearch, tgtCol, y_pred_prob, tpr, fpr, roc_auc, recall, precision, prc_auc, mcc, searchCV = None, param_grid = None, plot = True):
    if not os.path.exists('PLOTS/{}/'.format(dir)):
        os.makedirs('PLOTS/{}/'.format(dir))
    plt.hist(y_pred_prob, bins=50)
    plt.xlabel('Class Predicted')
    plt.savefig('PLOTS/{}/y_pred_prob_dist_{}.pdf'.format(dir,tgtCol), bbox_inches='tight')
    plt.close()
    viewParams = ''
    if gridSearch:
        feat_imp = pd.Series(searchCV.best_estimator_.named_steps["lnr"].get_booster().get_fscore()).sort_values(ascending=False)
        feat_imp.plot(kind='bar', title='Feature Importances')
        plt.ylabel('Feature Importance Score')
        plt.xticks(fontsize=round(300./feat_imp.size))
        plt.savefig('PLOTS/{}/featImportance_{}.pdf'.format(dir,tgtCol), bbox_inches='tight')
        plt.close()
        scanParam = sorted([(k,len(l)) for k,l in param_grid[0].items()], key = lambda x: x[1])[-1][0]
        print('Scanned parameter: {}'.format(scanParam))
        GridSearch_table_plot(searchCV, scanParam, 'PLOTS/{}/gridScore_{}.pdf'.format(dir,tgtCol), graph=plot, negative=False, display_all_params=False)
        for i in searchCV.best_params_.items():
            viewParams+= '{} \n'.format(i)
    lw = 2
    plt.plot(fpr, tpr, color='darkred',lw=lw, label='ROC curve (area = {}), MCC = {}%'.format(round(roc_auc,3),round(100.*mcc,2)))
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic {}\n'.format(tgtCol) + viewParams)
    plt.legend(loc="lower right")
    plt.savefig('PLOTS/{}/ROC_{}.pdf'.format(dir,tgtCol), bbox_inches='tight')
    plt.close()
    plt.plot(recall,precision, color='darkred',lw=lw, label='PRC curve (area = {}), MCC = {}%'.format(round(prc_auc,3),round(100.*mcc,2)))
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.title('Precision Recall Curve {}\n'.format(tgtCol) + viewParams)
    plt.legend(loc="upper right")
    plt.savefig('PLOTS/{}/PRC_{}.pdf'.format(dir,tgtCol), bbox_inches='tight')
    plt.close()


def printScoresBinaryClassifier(y_test,y_pred_prob,y_pred):
    # metrics from probilities and predicted class
    precision, recall, _ = precision_recall_curve(y_test, y_pred_prob, pos_label=1)
    prc_auc = auc(recall,precision)
    print('PRC AUC: {}'.format(prc_auc))

    mcc = matthews_corrcoef(y_test, y_pred)
    print('MCC: {}'.format(mcc))

    fpr, tpr, thresholds = roc_curve(y_test, y_pred_prob, pos_label=1)
    roc_auc = auc(fpr, tpr)
    print('ROC AUC: {}'.format(roc_auc))

    y_correct = y_pred == y_test
    accuracy = y_correct.sum()/y_correct.size
    print('Total Accuracy: ' + str(accuracy))

    vcTruthDict=dict(pd.Series(y_test).value_counts())
    vcTruthDictFr=dict(pd.Series(y_test).value_counts(normalize=True))
    vcPredDict=dict(pd.Series(y_pred).value_counts())
    vcPredDictFr=dict(pd.Series(y_pred).value_counts(normalize=True))
    print('Value Counts:')
    for k in vcPredDict.keys():
        print('{} Truth: {}, {}%'.format(k,vcTruthDict[k],round(100.*vcTruthDictFr[k],3)))
        print('{} Predicted {}, {}%'.format(k,vcPredDict[k],round(100.*vcPredDictFr[k],3)))

    print('___test_results___')
    print(confusion_matrix(y_test, y_pred))
    print(classification_report(y_test, y_pred))

    # make random number guess with predicted proportion of segments to directly compare preformance
    y_rand = [np.random.random() for i in range(len(y_pred))]
    try:
        y_rand = np.array([x<vcPredDictFr[1] for x in y_rand])
    except KeyError:
        y_rand = np.array([0. for x in y_rand])
    print(' ')
    print('___random_results__')
    print(confusion_matrix(y_test, y_rand))
    print(classification_report(y_test, y_rand))
    return tpr, fpr, roc_auc, recall, precision, prc_auc, mcc
